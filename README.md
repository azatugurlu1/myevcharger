# MyEvCharger

My EV Charger is a platform that help you charge your electric vehicle easily.

## Architecture

Clean Architecture (Layered) is being followed in this project.

Layers are: 
    
    - Data
        - Repositories
    - Domain
        - Entites
        - Usecases
    - Presentation
        - Views and others

Having a clear architecture helps with:
    
    - Provides basis for re-use of elements and decisions. This saves design costs and mitigiate the risk of design mistakes.
    - Scalability and performance
    - Faciliates communication with stakeholders and co workers
    
    If standard architecture is followed: 
    - Reduce time to start for new developers/outsorce
    - Good practices and help on common confusions/problems available


In Presentation layer MVVM with State have been used. Having full control of UI with state makes life a lot easier.

## Unit Test
 
 Repositories, Usecases and ViewModels are unit tested.
 Test names has a pattern and it is nice to follow same pattern for future tests.


## Dependency Injection

Dependency Injection class is a basic class that manages dependencies. Implemented by myself.
As project grows, this helps maintain and testing.


## Comments

I believe code should be self explained and there should be no need for comments.
Also, comments are easily get obsolete and bring more maintain and confusion.

## NOTE
In my location station api is returning empty array. I have added mock data in StationsReposity. Please return mocks if you want to see UI with data.
