package com.azat.myevcharger.presentation.stations.viewholder

import android.annotation.SuppressLint
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.azat.myevcharger.databinding.StationViewHolderBinding
import com.azat.myevcharger.di.Injection
import com.azat.myevcharger.domain.entities.Station

class StationViewHolder(private val stationViewHolderBinding: StationViewHolderBinding):
    RecyclerView.ViewHolder(stationViewHolderBinding.root) {

    private var stationViewHolderViewModel = StationViewHolderViewModel(Injection)

    init {
        stationViewHolderViewModel.viewState.observe(itemView.context as LifecycleOwner) {
            it?.let { render(it) }
        }
    }

    private fun render(viewState: StationViewHolderViewModelState) {
        when(viewState) {
            is StationViewHolderViewModelState.Loaded -> {
                stationViewHolderBinding.textViewLocationName.text = viewState.locationName
                stationViewHolderBinding.textViewAddress.text = viewState.address
                stationViewHolderBinding.textViewDistanceLabel.visibility = viewState.distanceVisibility
                stationViewHolderBinding.imageViewArrow.visibility = viewState.distanceVisibility
                stationViewHolderBinding.textViewConnector1.visibility = viewState.connector1Visibility
                stationViewHolderBinding.textViewConnector2.visibility = viewState.connector2Visibility
                stationViewHolderBinding.textViewConnector3.visibility = viewState.connector3Visibility
                stationViewHolderBinding.textViewConnector4.visibility = viewState.connector4Visibility
                stationViewHolderBinding.textViewConnector1.text = viewState.connector1Text
                stationViewHolderBinding.textViewConnector2.text = viewState.connector2Text
                stationViewHolderBinding.textViewConnector3.text = viewState.connector3Text
                stationViewHolderBinding.textViewConnector4.text = viewState.connector4Text

            }
        }
    }

    fun bind(station: Station) {
        stationViewHolderViewModel.onBindItem(station)
    }
}