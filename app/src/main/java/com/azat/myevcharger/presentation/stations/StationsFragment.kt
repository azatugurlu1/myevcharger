package com.azat.myevcharger.presentation.stations

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.azat.myevcharger.R
import com.azat.myevcharger.databinding.FragmentStationsBinding
import com.azat.myevcharger.di.Injection
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.google.android.material.snackbar.Snackbar


class StationsFragment : Fragment() {

    private var _binding: FragmentStationsBinding? = null
    private val binding get() = _binding!!
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var stationsViewModel: StationsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStationsBinding.inflate(inflater, container, false)
        binding.recyclerViewStations.layoutManager = LinearLayoutManager(requireContext())
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        stationsViewModel = StationsViewModel(Injection)
        stationsViewModel.viewState.observe(viewLifecycleOwner) {
            it?.let { render(it) }
        }
        stationsViewModel.onCreateView()
        getLocation(object : LocationCallBack {
            override fun onLocationFetchedSuccess(location: Location) {
                stationsViewModel.onLocationFetched(location)
            }

            override fun onLocationFetchedFailed(error: String) {
                Snackbar.make(binding.root, error, 3000).show()
            }
        })

        binding.floatingActionButtonSettings.setOnClickListener {
            findNavController().navigate(R.id.action_stationsFragment_to_settingsFragment)
        }

        return binding.root
    }

    private fun render(viewState: StationsViewModelState) {
        binding.progressBarLoading.visibility = View.GONE
        when(viewState) {
            is StationsViewModelState.Loading -> {
                binding.progressBarLoading.visibility = View.VISIBLE
                binding.textViewNoData.visibility = View.GONE
                binding.imageViewNoData.visibility = View.GONE
            }
            is StationsViewModelState.Loaded -> {
                binding.recyclerViewStations.visibility = View.VISIBLE
                binding.textViewNoData.visibility = View.GONE
                binding.imageViewNoData.visibility = View.GONE
                binding.recyclerViewStations.adapter = StationViewHolderAdapter(viewState.stations)
            }
            is StationsViewModelState.Error -> {
                Snackbar.make(binding.root, viewState.message, 3000).show()
            }
            StationsViewModelState.NoDataToShow -> {
                binding.textViewNoData.visibility = View.VISIBLE
                binding.imageViewNoData.visibility = View.VISIBLE
            }
        }
    }

    private fun getLocation(locationCallBack: LocationCallBack) {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestLocationPermissions(locationCallBack)
        } else {
            fusedLocationClient.getCurrentLocation(Priority.PRIORITY_HIGH_ACCURACY, object : CancellationToken() {
                override fun onCanceledRequested(p0: OnTokenCanceledListener) = CancellationTokenSource().token

                override fun isCancellationRequested() = false
            })
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        locationCallBack.onLocationFetchedSuccess(location)
                    } else {
                        locationCallBack
                            .onLocationFetchedFailed("Getting location is failed.")
                    }

                }
        }
    }

    private fun requestLocationPermissions(locationCallBack: LocationCallBack) {
        val locationPermissionRequest = this.registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            when {
                permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                    getLocation(locationCallBack)
                }
                permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                    getLocation(locationCallBack)
                } else -> {
                    binding.progressBarLoading.visibility = View.GONE
                    locationCallBack.onLocationFetchedFailed("Location permissions denied, please give location permissions to continue.")
                }
            }
        }

        locationPermissionRequest.launch(arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION))
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}