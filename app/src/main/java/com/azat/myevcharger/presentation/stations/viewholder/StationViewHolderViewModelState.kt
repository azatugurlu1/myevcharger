package com.azat.myevcharger.presentation.stations.viewholder

sealed class StationViewHolderViewModelState {
    data class Loaded(
        val locationName: String,
        val address: String,
        val kWVisibility: Int,
        val distanceVisibility: Int,
        val connector1Visibility: Int,
        val connector1Text: String,
        val connector2Visibility: Int,
        val connector2Text: String,
        val connector3Visibility: Int,
        val connector3Text: String,
        val connector4Visibility: Int,
        val connector4Text: String,
        ): StationViewHolderViewModelState()
}