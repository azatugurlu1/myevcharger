package com.azat.myevcharger.presentation.login

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack
import com.azat.myevcharger.di.InjectionInterface

class LoginViewModel(private val injection: InjectionInterface): ViewModel(), AuthCallBack {
    val viewState: MutableLiveData<LoginViewModelState> = MutableLiveData()

    fun onCreateView() {
        viewState.value = LoginViewModelState.Initial
    }

    fun passwordTextChanged(newText: String) {
        if (newText.isNotEmpty()) {
            viewState.value = LoginViewModelState.UpdatePasswordClearIcon(View.VISIBLE)
        } else {
            viewState.value = LoginViewModelState.UpdatePasswordClearIcon(View.GONE)
        }

    }

    fun clearPasswordClicked() {
        viewState.value = LoginViewModelState.ClearPassword
    }

    fun doLogin(username: String, password: String) {
        viewState.value = LoginViewModelState.Loading
        injection.loginUseCases.login(username, password, this)
    }

    override fun authSuccess(token: String) {
        viewState.value = LoginViewModelState.Loaded
        injection.sharedPreferenceUseCases.saveToken(token)
    }

    override fun authFailed(message: String) {
        viewState.value = LoginViewModelState.Error(message)
    }
}

