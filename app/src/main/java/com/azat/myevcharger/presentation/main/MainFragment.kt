package com.azat.myevcharger.presentation.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.azat.myevcharger.R
import com.azat.myevcharger.databinding.FragmentMainBinding
import com.azat.myevcharger.di.Injection

class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Injection.sharedPreferenceUseCases.getToken().isEmpty()) {
            findNavController().navigate(R.id.action_mainFragment_to_loginFragment)
        } else {
            findNavController().navigate(R.id.action_mainFragment_to_stationsFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}