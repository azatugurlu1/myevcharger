package com.azat.myevcharger.presentation.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.azat.myevcharger.R
import com.azat.myevcharger.databinding.FragmentLoginBinding
import com.azat.myevcharger.di.Injection
import com.google.android.material.snackbar.Snackbar


class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        loginViewModel = LoginViewModel(Injection)
        loginViewModel.viewState.observe(viewLifecycleOwner) {
            it?.let { render(it) }
        }
        loginViewModel.onCreateView()
        return binding.root
    }

    private fun render(viewState: LoginViewModelState) {
        when(viewState) {
            is LoginViewModelState.Initial -> {
                setUpListeners()
            }
            is LoginViewModelState.Loading -> {
                binding.progressBarLoading.visibility = View.VISIBLE
            }
            is LoginViewModelState.Loaded -> {
                binding.progressBarLoading.visibility = View.GONE
                findNavController().navigate(R.id.action_loginFragment_to_stationsFragment)
            }
            is LoginViewModelState.Error -> {
                binding.progressBarLoading.visibility = View.GONE
                Snackbar.make(binding.root, viewState.message, 1000).show()
            }
            is LoginViewModelState.UpdatePasswordClearIcon -> {
                binding.imageViewClearPassword.visibility = viewState.visibility
            }
            LoginViewModelState.ClearPassword -> {
                binding.editTextTextPassword.text.clear()
            }
        }
    }

    private fun setUpListeners() {
        binding.buttonLogIn.setOnClickListener {
            loginViewModel.doLogin(binding.editTextTextEmailAddress.text.toString(), binding.editTextTextPassword.text.toString())
        }

        binding.editTextTextPassword.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                loginViewModel.passwordTextChanged(p0.toString())
            }
        })

        binding.imageViewClearPassword.setOnClickListener {
            loginViewModel.clearPasswordClicked()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}