package com.azat.myevcharger.presentation.stations.viewholder

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azat.myevcharger.di.InjectionInterface
import com.azat.myevcharger.domain.entities.Station

class StationViewHolderViewModel(private val injectionInterface: InjectionInterface): ViewModel() {
    val viewState: MutableLiveData<StationViewHolderViewModelState> = MutableLiveData()

    fun onBindItem(station: Station) {
        var distanceVisibility = View.VISIBLE
        if (injectionInterface.sharedPreferenceUseCases.isDistanceLabelHidden()) {
            distanceVisibility = View.GONE
        }
        var kwVisibility = View.VISIBLE
        if (injectionInterface.sharedPreferenceUseCases.isKwLabelHidden()) {
            kwVisibility = View.GONE
        }

        var connector1Visibility = View.GONE
        var connector1Text = ""
        var connector2Visibility = View.GONE
        var connector2Text = ""
        var connector3Visibility = View.GONE
        var connector3Text = ""
        var connector4Visibility = View.GONE
        var connector4Text = ""

        if (!injectionInterface.sharedPreferenceUseCases.isKwLabelHidden() &&
            station.evses.isNotEmpty()) {
            val connectors = station.evses[0].connectors
            if (connectors.isNotEmpty()) {
                connector1Visibility = View.VISIBLE
                connector1Text = "${connectors[0].maxKw}\nkW"
            }

            if (connectors.size > 1) {
                connector2Visibility = View.VISIBLE
                connector2Text = "${connectors[1].maxKw}\nkW"
            }

            if (connectors.size > 2) {
                connector3Visibility = View.VISIBLE
                connector3Text = "${connectors[2].maxKw}\nkW"
            }

            if (connectors.size > 3) {
                connector4Visibility = View.VISIBLE
                connector4Text = "${connectors[3].maxKw}\nkW"
            }
        }

        viewState.value = StationViewHolderViewModelState.Loaded(
            station.name,
            "${station.address}, ${station.city}",
            kwVisibility,
            distanceVisibility,
            connector1Visibility,
            connector1Text,
            connector2Visibility,
            connector2Text,
            connector3Visibility,
            connector3Text,
            connector4Visibility,
            connector4Text)
    }
}