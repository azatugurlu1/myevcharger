package com.azat.myevcharger.presentation.login

sealed class LoginViewModelState {
    object Initial: LoginViewModelState()
    object Loading: LoginViewModelState()
    object Loaded : LoginViewModelState()
    data class Error(var message: String): LoginViewModelState()
    data class UpdatePasswordClearIcon(var visibility: Int): LoginViewModelState()
    object ClearPassword: LoginViewModelState()
}