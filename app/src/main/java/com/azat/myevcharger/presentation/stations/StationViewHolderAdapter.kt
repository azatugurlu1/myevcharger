package com.azat.myevcharger.presentation.stations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azat.myevcharger.databinding.StationViewHolderBinding
import com.azat.myevcharger.domain.entities.Station
import com.azat.myevcharger.presentation.stations.viewholder.StationViewHolder

class StationViewHolderAdapter(private val stations: List<Station>):
    RecyclerView.Adapter<StationViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): StationViewHolder {
        val itemBinding = StationViewHolderBinding.inflate(LayoutInflater.from(viewGroup.context),
            viewGroup, false)
        return StationViewHolder(itemBinding)
    }

    override fun onBindViewHolder(viewHolder: StationViewHolder, position: Int) {
        viewHolder.bind(stations[position])
    }

    override fun getItemCount() = stations.size
}