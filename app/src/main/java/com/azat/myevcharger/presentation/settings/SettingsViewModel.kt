package com.azat.myevcharger.presentation.settings

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azat.myevcharger.di.InjectionInterface

class SettingsViewModel(private val injection: InjectionInterface): ViewModel() {
    val viewState: MutableLiveData<SettingsViewModelState> = MutableLiveData()

    fun onCreateView() {
        val kwLabelHidden = injection.sharedPreferenceUseCases.isKwLabelHidden()
        val distanceLabelHidden = injection.sharedPreferenceUseCases.isDistanceLabelHidden()
        viewState.value = SettingsViewModelState.Loaded(kwLabelHidden, distanceLabelHidden)
    }

    fun onKwLabelHiddenSwitchChanged(isChecked: Boolean) {
        injection.sharedPreferenceUseCases.saveKwLabelHiddenStatus(isChecked)
    }

    fun onDistanceLabelHiddenSwitchChanged(isChecked: Boolean) {
        injection.sharedPreferenceUseCases.saveDistanceLabelHiddenStatus(isChecked)
    }
}