package com.azat.myevcharger.presentation.settings

sealed class SettingsViewModelState {
    data class Loaded(var isKwLabelChecked: Boolean,
                      var isDistanceLabelChecked: Boolean): SettingsViewModelState()
}