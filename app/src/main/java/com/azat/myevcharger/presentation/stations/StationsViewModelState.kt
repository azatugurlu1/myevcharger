package com.azat.myevcharger.presentation.stations

import com.azat.myevcharger.domain.entities.Station

sealed class StationsViewModelState {
    object NoDataToShow: StationsViewModelState()
    object Loading: StationsViewModelState()
    data class Loaded(var stations: List<Station>): StationsViewModelState()
    data class Error(var message: String): StationsViewModelState()
}