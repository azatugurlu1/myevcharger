package com.azat.myevcharger.presentation.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.azat.myevcharger.databinding.FragmentSettingsBinding
import com.azat.myevcharger.di.Injection


class SettingsFragment: Fragment() {

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!
    private lateinit var settingsViewModel: SettingsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        settingsViewModel = SettingsViewModel(Injection)
        settingsViewModel.viewState.observe(viewLifecycleOwner) {
            it?.let { render(it) }
        }
        settingsViewModel.onCreateView()
        setUpListeners()
        return binding.root
    }


    private fun render(viewState: SettingsViewModelState) {
        when(viewState) {
            is SettingsViewModelState.Loaded -> {
                binding.switchHideKw.isChecked = viewState.isKwLabelChecked
                binding.switchHideDistance.isChecked = viewState.isDistanceLabelChecked
            }
        }
    }


    private fun setUpListeners() {
        binding.switchHideKw.setOnCheckedChangeListener { _, isChecked ->
            settingsViewModel.onKwLabelHiddenSwitchChanged(isChecked)
        }
        binding.switchHideDistance.setOnCheckedChangeListener { _, isChecked ->
            settingsViewModel.onDistanceLabelHiddenSwitchChanged(isChecked)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}