package com.azat.myevcharger.presentation.stations

import android.location.Location

interface LocationCallBack {
    fun onLocationFetchedSuccess(location: Location)
    fun onLocationFetchedFailed(error: String)
}