package com.azat.myevcharger.presentation.stations

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack
import com.azat.myevcharger.di.InjectionInterface
import com.azat.myevcharger.domain.entities.Station

class StationsViewModel(private val injection: InjectionInterface): ViewModel(), StationsCallBack {
    val viewState: MutableLiveData<StationsViewModelState> = MutableLiveData()

    fun onCreateView() {
        viewState.value = StationsViewModelState.Loading
    }

    fun onLocationFetched(location: Location) {
        getStations(location)
    }

    private fun getStations(location: Location) {
        injection.stationsUseCases.getStations(location.latitude, location.longitude, this)
    }

    override fun stationsFetchedSuccess(stations: List<Station>) {
        if (stations.isEmpty()) {
            viewState.value = StationsViewModelState.NoDataToShow
        } else {
            viewState.value = StationsViewModelState.Loaded(stations)
        }
    }

    override fun stationsFetchedFailed(message: String) {
       viewState.value = StationsViewModelState.Error(message)
    }
}