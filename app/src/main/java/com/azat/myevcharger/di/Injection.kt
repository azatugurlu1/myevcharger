package com.azat.myevcharger.di

import android.content.SharedPreferences
import com.azat.myevcharger.data.remote.Server
import com.azat.myevcharger.data.repositories.*
import com.azat.myevcharger.domain.usecases.*

object Injection: InjectionInterface {

    override val loginUseCases: LoginUseCasesInterface by lazy {
        LoginUseCases(loginRepository)
    }
    override val stationsUseCases: StationsUseCasesInterface by lazy {
        StationsUseCases(stationsRepository)
    }
    override val sharedPreferenceUseCases: SharedPreferencesUseCasesInterface
        get() = SharedPreferencesUseCases(sharedPreferenceRepository)

    private val loginRepository: LoginRepositoryInterface
        get() = LoginRepository(Server)

    private val stationsRepository: StationsRepositoryInterface
        get() = StationsRepository(Server)

    fun setSharedPreferences(sharedPreferences: SharedPreferences) {
        sharedPreferenceRepository = SharedPreferenceRepository(sharedPreferences)
    }

    private lateinit var sharedPreferenceRepository: SharedPreferenceRepositoryInterface
}
