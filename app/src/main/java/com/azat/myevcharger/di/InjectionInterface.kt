package com.azat.myevcharger.di

import com.azat.myevcharger.domain.usecases.LoginUseCasesInterface
import com.azat.myevcharger.domain.usecases.SharedPreferencesUseCasesInterface
import com.azat.myevcharger.domain.usecases.StationsUseCasesInterface

interface InjectionInterface {
    val loginUseCases: LoginUseCasesInterface
    val stationsUseCases: StationsUseCasesInterface
    val sharedPreferenceUseCases: SharedPreferencesUseCasesInterface
}
