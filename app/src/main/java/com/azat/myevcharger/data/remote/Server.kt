package com.azat.myevcharger.data.remote

import com.azat.myevcharger.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Server: ServerInterface {
    private const val baseUrl = "https://apitest.virta.fi/"
    private lateinit var endPoints: EndPoints
    private lateinit var token: String

    private fun getConverterFactory(): Converter.Factory {
        val gson = GsonBuilder().serializeNulls().setLenient().create()
        return GsonConverterFactory.create(gson)
    }

    init {
        setup("")
    }

    override fun setup(token: String) {
        Server.token = token
        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(getConverterFactory())

        val httpClientBuilder = OkHttpClient.Builder().addInterceptor(Interceptor { chain ->
            val newRequest: Request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
            chain.proceed(newRequest)
        })
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)

        val logging = HttpLoggingInterceptor()

        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        httpClientBuilder.addInterceptor(logging)

        val okHttpClient: OkHttpClient = httpClientBuilder.build()

        retrofitBuilder.client(okHttpClient)
        val retrofit = retrofitBuilder.build()
        val endPoints = retrofit.create(EndPoints::class.java)
        Server.endPoints = endPoints
    }

    override fun getEndpoints(): EndPoints {
        return endPoints
    }
}