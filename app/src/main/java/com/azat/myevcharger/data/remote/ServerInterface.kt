package com.azat.myevcharger.data.remote

interface ServerInterface {
    fun getEndpoints(): EndPoints
    fun setup(token: String)
}