package com.azat.myevcharger.data.repositories

import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack

interface StationsRepositoryInterface {
    fun getStations(latitude: Double, longitude: Double, callBack: StationsCallBack)
}