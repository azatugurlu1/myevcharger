package com.azat.myevcharger.data.remote

import com.google.gson.annotations.SerializedName

data class LoginPostData(
    var email: String,
    @SerializedName("code") var password: String
)
