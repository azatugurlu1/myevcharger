package com.azat.myevcharger.data.repositories

import com.azat.myevcharger.data.remote.ServerInterface
import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack
import com.azat.myevcharger.di.Injection
import com.azat.myevcharger.domain.entities.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StationsRepository(private val serverInterface: ServerInterface): StationsRepositoryInterface {
    override fun getStations(
        latitude: Double,
        longitude: Double,
        callBack: StationsCallBack
    ) {
        val endpoints = serverInterface.getEndpoints()
        val call = endpoints.getStations(latitude-5, latitude+5, longitude-5, longitude+5, 10)
        call.enqueue(object : Callback<List<Station>> {
            override fun onResponse(call: Call<List<Station>>, response: Response<List<Station>>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callBack.stationsFetchedSuccess(response.body()!!)
                        //callBack.stationsFetchedSuccess(getStations())
                    } else {
                        callBack.stationsFetchedFailed("Error")
                    }
                } else {
                    val error = JSONObject(response.errorBody()!!.string())
                    callBack.stationsFetchedFailed(error.getString("message"))
                }
            }

            override fun onFailure(call: Call<List<Station>>, t: Throwable) {
                callBack.stationsFetchedFailed(t.localizedMessage as String)
            }
        })
    }


    private fun getStations(): List<Station> {
        val connector1 = Connector(1234, ConnectorType.CCS, CurrentType.AC, 22)
        val evse1 = Evse(127033, listOf(connector1, connector1, connector1, connector1), true, true, listOf(), true, 1,5)
        val station1 = Station(127033,
            "BIP P&R Leopoldau Station 2",
            48.278067,
            16.456204,
            0,
            "some Address",
            "Wien",
            "",
            "Hubject",
            listOf(),
            listOf(evse1, evse1)
        )

        val station2 = Station(127035,
            "EnBW Ladestation 3175_1",
            48.278067,
            16.456204,
            0,
            "some new Address",
            "Esslingen",
            "",
            "Hubject",
            listOf(),
            listOf(evse1, evse1)
        )

        return listOf(station1, station2)
    }
}