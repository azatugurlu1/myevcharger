package com.azat.myevcharger.data.repositories

import com.azat.myevcharger.data.remote.LoginPostData
import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack

interface LoginRepositoryInterface {
    fun login(loginPostData: LoginPostData, authCallBack: AuthCallBack)
}