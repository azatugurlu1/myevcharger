package com.azat.myevcharger.data.remote

import com.azat.myevcharger.domain.entities.Station
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface EndPoints {

    @POST("/v4/auth")
    fun login(@Body credentials: LoginPostData): Call<JsonObject>

    @DELETE("/v4/auth")
    fun logout(): Call<JsonObject>

    @GET("/v4/stations")
    fun getStations(@Query("latMin") latMin: Double,
                    @Query("latMax") latMax: Double,
                    @Query("longMin") longMin: Double,
                    @Query("longMax") longMax: Double,
                    @Query("limit") limit: Int
    ): Call<List<Station>>
}