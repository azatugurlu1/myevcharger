package com.azat.myevcharger.data.repositories.callbacks

interface AuthCallBack {
    fun authSuccess(token: String)
    fun authFailed(message: String)
}