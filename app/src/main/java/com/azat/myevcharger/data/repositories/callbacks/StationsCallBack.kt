package com.azat.myevcharger.data.repositories.callbacks

import com.azat.myevcharger.domain.entities.Station

interface StationsCallBack {
    fun stationsFetchedSuccess(stations: List<Station>)
    fun stationsFetchedFailed(message: String)
}