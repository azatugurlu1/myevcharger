package com.azat.myevcharger.data.repositories

import android.content.SharedPreferences

class SharedPreferenceRepository(
    private val sharedPreferences: SharedPreferences): SharedPreferenceRepositoryInterface {

    private val accessToken = "access_token"
    private val hideKwLabel = "hide_kw_label"
    private val hideDistanceLabel = "hide_distance_label"
    private val editor = sharedPreferences.edit()


    override fun getToken(): String {
        return sharedPreferences.getString(accessToken, "").toString()
    }

    override fun isKwLabelHidden(): Boolean {
        return sharedPreferences.getBoolean(hideKwLabel, false)
    }

    override fun isDistanceLabelHidden(): Boolean {
        return sharedPreferences.getBoolean(hideDistanceLabel, false)
    }

    override fun saveToken(token: String) {
        editor.putString(accessToken, token)
        editor.apply()
    }

    override fun saveKwLabelHiddenStatus(isHidden: Boolean) {
        editor.putBoolean(hideKwLabel, isHidden)
        editor.apply()
    }

    override fun saveDistanceLabelHiddenStatus(isHidden: Boolean) {
        editor.putBoolean(hideDistanceLabel, isHidden)
        editor.apply()
    }

    override fun clearPreferences() {
        editor.clear()
        editor.apply()
    }
}