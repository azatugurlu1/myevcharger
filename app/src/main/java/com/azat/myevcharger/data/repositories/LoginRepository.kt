package com.azat.myevcharger.data.repositories

import com.azat.myevcharger.data.remote.LoginPostData
import com.azat.myevcharger.data.remote.ServerInterface
import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class LoginRepository(private val serverInterface: ServerInterface): LoginRepositoryInterface {
    override fun login(loginPostData: LoginPostData, authCallBack: AuthCallBack) {
        val endpoints = serverInterface.getEndpoints()
        val call = endpoints.login(loginPostData)
        call.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    val token = response.body()?.get("access_token").toString()
                    serverInterface.setup(token)
                    authCallBack.authSuccess(token)
                } else {
                    val error = JSONObject(response.errorBody()!!.string())
                    authCallBack.authFailed(error.getString("message"))
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                authCallBack.authFailed(t.localizedMessage as String)
            }
        })
    }
}