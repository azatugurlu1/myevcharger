package com.azat.myevcharger

import android.app.Application
import android.content.Context
import com.azat.myevcharger.di.Injection

class MyEvChargerApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        val sharedPreferences =
            applicationContext.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
        Injection.setSharedPreferences(sharedPreferences)
    }
}