package com.azat.myevcharger.domain.entities

enum class CurrentType {
    AC, DC
}
