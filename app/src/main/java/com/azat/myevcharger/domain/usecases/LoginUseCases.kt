package com.azat.myevcharger.domain.usecases
import com.azat.myevcharger.data.remote.LoginPostData
import com.azat.myevcharger.data.repositories.LoginRepositoryInterface
import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack

class LoginUseCases(private val loginRepository: LoginRepositoryInterface): LoginUseCasesInterface {
    override fun login(email: String, password: String, authCallBack: AuthCallBack) {
        val loginPostData = LoginPostData(email = email, password = password)
        loginRepository.login(loginPostData, authCallBack = authCallBack)
    }
}