package com.azat.myevcharger.domain.usecases

import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack

interface LoginUseCasesInterface {
    fun login(email: String, password: String, authCallBack: AuthCallBack)
}