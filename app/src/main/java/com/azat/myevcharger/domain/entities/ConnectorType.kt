package com.azat.myevcharger.domain.entities

enum class ConnectorType {
    CCS, CHAdeMO, Type2, Type3, Shuko
}
