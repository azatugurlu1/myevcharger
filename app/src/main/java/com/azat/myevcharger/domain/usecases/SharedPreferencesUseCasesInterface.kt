package com.azat.myevcharger.domain.usecases

interface SharedPreferencesUseCasesInterface {
    fun getToken(): String
    fun isKwLabelHidden(): Boolean
    fun isDistanceLabelHidden(): Boolean
    fun saveToken(token: String)
    fun saveKwLabelHiddenStatus(isHidden: Boolean)
    fun saveDistanceLabelHiddenStatus(isHidden: Boolean)
    fun clearPreferences()
}