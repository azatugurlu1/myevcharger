package com.azat.myevcharger.domain.usecases

import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack

interface StationsUseCasesInterface {
    fun getStations(latitude: Double, longitude: Double, callBack: StationsCallBack)
}