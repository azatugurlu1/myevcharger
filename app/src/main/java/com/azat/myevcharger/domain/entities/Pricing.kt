package com.azat.myevcharger.domain.entities

data class Pricing(
    val name: String,
    val priceCents: Int,
    val currency: String,
    val minutes: Int
)
