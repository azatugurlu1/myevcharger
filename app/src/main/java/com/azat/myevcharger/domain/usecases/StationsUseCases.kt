package com.azat.myevcharger.domain.usecases

import com.azat.myevcharger.data.repositories.StationsRepositoryInterface
import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack

class StationsUseCases(private val stationsRepository: StationsRepositoryInterface)
    : StationsUseCasesInterface {
    override fun getStations(latitude: Double, longitude: Double, callBack: StationsCallBack) {
        stationsRepository.getStations(latitude, longitude, callBack)
    }
}