package com.azat.myevcharger.domain.usecases

import com.azat.myevcharger.data.repositories.SharedPreferenceRepositoryInterface

class SharedPreferencesUseCases(
    private val sharedPreferencesRepository: SharedPreferenceRepositoryInterface):
    SharedPreferencesUseCasesInterface {
    override fun getToken(): String {
        return sharedPreferencesRepository.getToken()
    }

    override fun isKwLabelHidden(): Boolean {
        return sharedPreferencesRepository.isKwLabelHidden()
    }

    override fun isDistanceLabelHidden(): Boolean {
        return sharedPreferencesRepository.isDistanceLabelHidden()
    }

    override fun saveToken(token: String) {
        sharedPreferencesRepository.saveToken(token)
    }

    override fun saveKwLabelHiddenStatus(isHidden: Boolean) {
        sharedPreferencesRepository.saveKwLabelHiddenStatus(isHidden)
    }

    override fun saveDistanceLabelHiddenStatus(isHidden: Boolean) {
        sharedPreferencesRepository.saveDistanceLabelHiddenStatus(isHidden)
    }

    override fun clearPreferences() {
        sharedPreferencesRepository.clearPreferences()
    }
}