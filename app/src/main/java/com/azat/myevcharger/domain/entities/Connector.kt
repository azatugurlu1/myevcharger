package com.azat.myevcharger.domain.entities

data class Connector(
    val connectorID: Int,
    val type: ConnectorType,
    val currentType: CurrentType,
    val maxKw: Int
)
