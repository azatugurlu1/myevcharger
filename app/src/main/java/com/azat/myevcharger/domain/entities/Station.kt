package com.azat.myevcharger.domain.entities

data class Station(
    val id: Int,
    val name: String,
    val latitude: Double,
    val longtitude: Double,
    val icon: Int,
    val address: String,
    val city: String,
    val openHours: String,
    val provider: String,
    val pictures: List<String>,
    val evses: List<Evse>
)
