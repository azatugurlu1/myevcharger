package com.azat.myevcharger.domain.entities

data class Evse(
    val id: Int,
    val connectors: List<Connector>,
    val available: Boolean,
    val reservable: Boolean,
    val pricing: List<Pricing>,
    val oneTimePayment: Boolean,
    val status: Int,
    val minutesWithoutTimeCharge: Int
)
