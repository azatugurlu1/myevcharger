package com.azat.myevcharger.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.azat.myevcharger.di.TestInjection
import com.azat.myevcharger.presentation.settings.SettingsViewModel
import com.azat.myevcharger.presentation.settings.SettingsViewModelState
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class SettingsViewModelTest {
    private lateinit var settingsViewModel: SettingsViewModel

    private var injectionInterface = TestInjection

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        settingsViewModel = SettingsViewModel(injectionInterface)
    }

    @Test
    fun testOnCreateViewShouldCallSharedPreferencesUseCases() {
        settingsViewModel.onCreateView()
        verify(injectionInterface.sharedPreferenceUseCases).isKwLabelHidden()
        verify(injectionInterface.sharedPreferenceUseCases).isDistanceLabelHidden()
    }

    @Test
    fun testOnCreateViewShouldSetViewModelStateLoaded() {
        whenever(injectionInterface.sharedPreferenceUseCases.isKwLabelHidden()).thenReturn(true)
        whenever(injectionInterface.sharedPreferenceUseCases.isDistanceLabelHidden()).thenReturn(true)
        settingsViewModel.onCreateView()
        assert(settingsViewModel.viewState.value == SettingsViewModelState.Loaded(true, true))
    }

    @Test
    fun testOnKwLabelHiddenSwitchChangedShouldSharedPreferencesUseCases() {
        settingsViewModel.onKwLabelHiddenSwitchChanged(true)
        verify(injectionInterface.sharedPreferenceUseCases).saveKwLabelHiddenStatus(true)
    }

    @Test
    fun testOnDistanceLabelHiddenSwitchChangedShouldSharedPreferencesUseCases() {
        settingsViewModel.onDistanceLabelHiddenSwitchChanged(true)
        verify(injectionInterface.sharedPreferenceUseCases).saveDistanceLabelHiddenStatus(true)
    }


}