package com.azat.myevcharger.presentation

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.azat.myevcharger.presentation.login.LoginViewModel
import com.azat.myevcharger.presentation.login.LoginViewModelState
import com.azat.myevcharger.di.TestInjection
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.verify

class LoginViewModelTest {
    private lateinit var loginViewModel: LoginViewModel
    private var injectionInterface = TestInjection

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        loginViewModel = LoginViewModel(injectionInterface)
    }

    @Test
    fun testOnViewCreateShouldSetViewModelStateInitial() {
        loginViewModel.onCreateView()
        assert(loginViewModel.viewState.value == LoginViewModelState.Initial)
    }

    @Test
    fun testOnLoginShouldCallUseCases() {
        loginViewModel.doLogin("test", "test")
        verify(injectionInterface.loginUseCases).login(any(), any(), any())
    }

    @Test
    fun testOnAuthSuccessShouldSetViewModelStateToLoaded() {
        loginViewModel.authSuccess("test")
        assert(loginViewModel.viewState.value == LoginViewModelState.Loaded)
    }

    @Test
    fun testOnAuthSuccessShouldSetSharedPreferenceUseCases() {
        loginViewModel.authSuccess("test")
        verify(injectionInterface.sharedPreferenceUseCases).saveToken("test")
    }

    @Test
    fun testOnAuthFailedShouldSetViewModelStateToError() {
        loginViewModel.authFailed("error")
        assert(loginViewModel.viewState.value == LoginViewModelState.Error("error"))
    }

    @Test
    fun testPasswordTextChangedShouldShowClearPasswordIcon() {
        loginViewModel.passwordTextChanged("test")
        assert(loginViewModel.viewState.value == LoginViewModelState.UpdatePasswordClearIcon(View.VISIBLE))
    }

    @Test
    fun testPasswordTextChangedShouldHideClearPasswordIcon() {
        loginViewModel.passwordTextChanged("")
        assert(loginViewModel.viewState.value == LoginViewModelState.UpdatePasswordClearIcon(View.GONE))
    }

    @Test
    fun testClearPasswordClickedShouldClearPassword() {
        loginViewModel.clearPasswordClicked()
        assert(loginViewModel.viewState.value == LoginViewModelState.ClearPassword)
    }
}