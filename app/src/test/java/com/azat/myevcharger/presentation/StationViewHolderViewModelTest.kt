package com.azat.myevcharger.presentation

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.azat.myevcharger.di.TestInjection
import com.azat.myevcharger.domain.entities.*
import com.azat.myevcharger.presentation.stations.viewholder.StationViewHolderViewModel
import com.azat.myevcharger.presentation.stations.viewholder.StationViewHolderViewModelState
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class StationViewHolderViewModelTest {
    private lateinit var stationViewHolderViewModel: StationViewHolderViewModel
    private var injectionInterface = TestInjection

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    val connector = Connector(1234, ConnectorType.CCS, CurrentType.AC, 22)
    val evse = Evse(127033, listOf(connector, connector, connector, connector), true,
        true, listOf(), true, 1,5)
    val station = Station(127033,
        "BIP P&R Leopoldau Station 2",
        48.278067,
        16.456204,
        0,
        "some Address",
        "Wien",
        "",
        "Hubject",
        listOf(),
        listOf(evse, evse)
    )

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        stationViewHolderViewModel = StationViewHolderViewModel(injectionInterface)
    }

    @Test
    fun testOnBindItemShouldSetViewModelStateLoaded() {
        whenever(injectionInterface.sharedPreferenceUseCases.isKwLabelHidden()).thenReturn(true)
        whenever(injectionInterface.sharedPreferenceUseCases.isDistanceLabelHidden()).thenReturn(true)
        stationViewHolderViewModel.onBindItem(station)
        assert(stationViewHolderViewModel.viewState.value ==
                StationViewHolderViewModelState.Loaded(
                    "BIP P&R Leopoldau Station 2",
                    "some Address, Wien",
                    View.GONE,
                    View.GONE,
                    View.GONE,
                    "",
                    View.GONE,
                    "",
                    View.GONE,
                    "",
                    View.GONE,
                    ""
                ))
    }

    @Test
    fun testOnBindItemShouldShowDistanceLabel() {
        whenever(injectionInterface.sharedPreferenceUseCases.isKwLabelHidden()).thenReturn(true)
        whenever(injectionInterface.sharedPreferenceUseCases.isDistanceLabelHidden()).thenReturn(false)
        stationViewHolderViewModel.onBindItem(station)
        assert(stationViewHolderViewModel.viewState.value ==
                StationViewHolderViewModelState.Loaded(
                    "BIP P&R Leopoldau Station 2",
                    "some Address, Wien",
                    View.GONE,
                    View.VISIBLE,
                    View.GONE,
                    "",
                    View.GONE,
                    "",
                    View.GONE,
                    "",
                    View.GONE,
                    ""
                ))
    }

    @Test
    fun testOnBindItemShouldShowKwLabel() {
        whenever(injectionInterface.sharedPreferenceUseCases.isKwLabelHidden()).thenReturn(false)
        whenever(injectionInterface.sharedPreferenceUseCases.isDistanceLabelHidden()).thenReturn(false)
        stationViewHolderViewModel.onBindItem(station)
        assert(stationViewHolderViewModel.viewState.value ==
                StationViewHolderViewModelState.Loaded(
                    "BIP P&R Leopoldau Station 2",
                    "some Address, Wien",
                    View.VISIBLE,
                    View.VISIBLE,
                    View.VISIBLE,
                    "22\nkW",
                    View.VISIBLE,
                    "22\nkW",
                    View.VISIBLE,
                    "22\nkW",
                    View.VISIBLE,
                    "22\nkW"
                ))
    }
}