package com.azat.myevcharger.presentation

import android.location.Location
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.azat.myevcharger.domain.entities.*
import com.azat.myevcharger.presentation.stations.StationsViewModel
import com.azat.myevcharger.presentation.stations.StationsViewModelState
import com.azat.myevcharger.di.TestInjection
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.verify

class StationsViewModelTest {
    private lateinit var stationsViewModel: StationsViewModel
    private var injectionInterface = TestInjection

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var location: Location

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        stationsViewModel = StationsViewModel(injectionInterface)
    }

    @Test
    fun testOnViewCreateShouldSetViewModelStateLoading() {
        stationsViewModel.onCreateView()
        assert(stationsViewModel.viewState.value == StationsViewModelState.Loading)
    }

    @Test
    fun testOnLocationFetchedShouldCallGetStations() {
        stationsViewModel.onLocationFetched(location)
        verify(injectionInterface.stationsUseCases).getStations(any(), any(), any())
    }

    @Test
    fun testOnStationsFetchedSuccessShouldSetViewModelStateToNoDataToShow() {
        stationsViewModel.stationsFetchedSuccess(listOf())
        assert(stationsViewModel.viewState.value == StationsViewModelState.NoDataToShow)
    }

    @Test
    fun testOnStationsFetchedSuccessShouldSetViewModelStateToLoaded() {
        val connector = Connector(1234, ConnectorType.CCS, CurrentType.AC, 22)
        val evse = Evse(127033, listOf(connector), true, true, listOf(),
            true, 1,5)
        val station = Station(127033,
            "BIP P&R Leopoldau Station 2",
            48.278067,
            16.456204,
            0,
            "some Address",
            "Wien",
            "",
            "Hubject",
            listOf(),
            listOf(evse)
        )
        stationsViewModel.stationsFetchedSuccess(listOf(station))
        assert(stationsViewModel.viewState.value == StationsViewModelState.Loaded(listOf(station)))
    }

    @Test
    fun testOnStationsFetchedFailedShouldSetViewModelStateToError() {
        stationsViewModel.stationsFetchedFailed("test")
        assert(stationsViewModel.viewState.value == StationsViewModelState.Error("test"))
    }
}