package com.azat.myevcharger.di

import com.azat.myevcharger.domain.usecases.LoginUseCasesInterface
import com.azat.myevcharger.domain.usecases.SharedPreferencesUseCasesInterface
import com.azat.myevcharger.domain.usecases.StationsUseCasesInterface
import org.mockito.kotlin.mock

object TestInjection: InjectionInterface {
    override val loginUseCases: LoginUseCasesInterface = mock()
    override val stationsUseCases: StationsUseCasesInterface = mock()
    override val sharedPreferenceUseCases: SharedPreferencesUseCasesInterface = mock()
}