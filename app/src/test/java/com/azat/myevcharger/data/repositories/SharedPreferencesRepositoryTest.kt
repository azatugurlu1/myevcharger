package com.azat.myevcharger.data.repositories

import android.content.SharedPreferences
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class SharedPreferencesRepositoryTest {
    @Mock
    private lateinit var sharedPreferences: SharedPreferences

    @Mock
    private lateinit var editor: SharedPreferences.Editor

    private lateinit var sharedPreferenceRepository: SharedPreferenceRepository

    private val accessToken = "access_token"
    private val hideKwLabel = "hide_kw_label"
    private val hideDistanceLabel = "hide_distance_label"


    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        whenever(sharedPreferences.edit()).thenReturn(editor)
        sharedPreferenceRepository = SharedPreferenceRepository(sharedPreferences)
    }

    @Test
    fun testGetTokenShouldReturnSavedToken() {
        whenever(sharedPreferences.getString(accessToken, "")).thenReturn("testToken")
        assert(sharedPreferenceRepository.getToken().equals("testToken"))
    }

    @Test
    fun testIsKwLabelHiddenShouldReturnTrue() {
        whenever(sharedPreferences.getBoolean(hideKwLabel, false)).thenReturn(true)
        assert(sharedPreferenceRepository.isKwLabelHidden())
    }

    @Test
    fun testDistanceLabelHiddenShouldReturnFalse() {
        whenever(sharedPreferences.getBoolean(hideDistanceLabel, false)).thenReturn(false)
        assert(!sharedPreferenceRepository.isDistanceLabelHidden())
    }

    @Test
    fun testSaveTokenShouldCallEditorWithString() {
        sharedPreferenceRepository.saveToken("testToken")
        verify(editor).putString(accessToken, "testToken")
    }

    @Test
    fun testSaveKwLabelHiddenStatusShouldCallEditorWithBoolean() {
        sharedPreferenceRepository.saveKwLabelHiddenStatus(true)
        verify(editor).putBoolean(hideKwLabel, true)
    }

    @Test
    fun testSaveDistanceLabelHiddenStatusShouldCallEditorWithBoolean() {
        sharedPreferenceRepository.saveDistanceLabelHiddenStatus(true)
        verify(editor).putBoolean(hideDistanceLabel, true)
    }

}