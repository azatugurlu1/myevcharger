package com.azat.myevcharger.data.repositories

import com.azat.myevcharger.data.remote.TestServer
import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.timeout
import org.mockito.kotlin.verify

class StationsRepositoryTest {
    @Mock
    private lateinit var stationsCallBack: StationsCallBack

    private lateinit var testServer: TestServer
    private lateinit var stationsRepository: StationsRepository

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        testServer = TestServer()
        testServer.setup("test")
        testServer.start()
        stationsRepository = StationsRepository(testServer)
    }

    @Test
    fun testGetStationsShouldSucceed() {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody("[]")
        testServer.setResponse(mockResponse)
        stationsRepository.getStations(60.12, 10.13, stationsCallBack)
        testServer.takeRequest()
        verify(stationsCallBack, timeout(1000)).stationsFetchedSuccess(any())
    }

    @Test
    fun testGetStationsShouldFail() {
        val mockResponse = MockResponse()
        testServer.setResponse(mockResponse)
        stationsRepository.getStations(60.12, 10.13, stationsCallBack)
        testServer.takeRequest()
        verify(stationsCallBack, timeout(1000)).stationsFetchedFailed(any())
    }
}
