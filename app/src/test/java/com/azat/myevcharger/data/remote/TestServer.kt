package com.azat.myevcharger.data.remote

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class TestServer: ServerInterface {
    private val mockWebServer = MockWebServer()
    private lateinit var endPoints: EndPoints

    fun start() {
        try {
            mockWebServer.start()
        } catch (ignored: Exception) {

        }

    }

    override fun getEndpoints(): EndPoints {
        return endPoints
    }

    fun takeRequest(): RecordedRequest {
        return mockWebServer.takeRequest()
    }

    fun takeRequest(timeOut: Long): RecordedRequest? {
        return mockWebServer.takeRequest(timeOut, TimeUnit.MILLISECONDS)
    }

    override fun setup(token: String) {
        val gson = GsonBuilder().setLenient().create()
        val client = OkHttpClient.Builder()
            .connectTimeout(50, TimeUnit.MILLISECONDS)
            .readTimeout(50, TimeUnit.MILLISECONDS)
        val retrofit = Retrofit.Builder()
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(mockWebServer.url("/").toString())
            .build()

        val endPoints = retrofit.create(EndPoints::class.java)
        this.endPoints = endPoints
    }

    fun setResponse(mockResponse: MockResponse) {
        mockWebServer.enqueue(mockResponse)
    }
}