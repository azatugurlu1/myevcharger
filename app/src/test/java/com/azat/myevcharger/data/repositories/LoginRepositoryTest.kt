package com.azat.myevcharger.data.repositories

import com.azat.myevcharger.data.remote.TestServer
import com.azat.myevcharger.data.remote.LoginPostData
import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.timeout
import org.mockito.kotlin.verify

class LoginRepositoryTest {
    @Mock
    private lateinit var authCallBack: AuthCallBack

    private lateinit var testServer: TestServer
    private lateinit var loginRepository: LoginRepository

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        testServer = TestServer()
        testServer.setup("test")
        testServer.start()
        loginRepository = LoginRepository(testServer)
    }

    @Test
    fun testLoginShouldSucceed() {
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody("{\"access_token\":\"testToken\"}")
        testServer.setResponse(mockResponse)
        val loginPostData = LoginPostData("test", "test")
        loginRepository.login(loginPostData, authCallBack)
        testServer.takeRequest()
        verify(authCallBack, timeout(1000)).authSuccess(any())
    }

    @Test
    fun testLoginShouldFail() {
        val mockResponse = MockResponse()
        testServer.setResponse(mockResponse)
        val loginPostData = LoginPostData("test", "test")
        loginRepository.login(loginPostData, authCallBack)
        testServer.takeRequest()
        verify(authCallBack, timeout(1000)).authFailed(any())
    }
}