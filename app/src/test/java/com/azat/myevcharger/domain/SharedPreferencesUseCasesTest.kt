package com.azat.myevcharger.domain

import com.azat.myevcharger.data.repositories.SharedPreferenceRepositoryInterface
import com.azat.myevcharger.domain.usecases.SharedPreferencesUseCases
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify

class SharedPreferencesUseCasesTest {
    @Mock
    private lateinit var sharedPreferenceRepository: SharedPreferenceRepositoryInterface

    private lateinit var sharedPreferencesUseCases: SharedPreferencesUseCases

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        sharedPreferencesUseCases = SharedPreferencesUseCases(sharedPreferenceRepository)
    }

    @Test
    fun testGetTokenShouldCallRepository() {
        sharedPreferencesUseCases.getToken()
        verify(sharedPreferenceRepository).getToken()
    }

    @Test
    fun testIsKwLabelHiddenShouldCallRepository() {
        sharedPreferencesUseCases.isKwLabelHidden()
        verify(sharedPreferenceRepository).isKwLabelHidden()
    }

    @Test
    fun testIsDistanceLabelHiddenShouldCallRepository() {
        sharedPreferencesUseCases.isDistanceLabelHidden()
        verify(sharedPreferenceRepository).isDistanceLabelHidden()
    }

    @Test
    fun testSaveTokenShouldCallRepository() {
        sharedPreferencesUseCases.saveToken("test")
        verify(sharedPreferenceRepository).saveToken("test")
    }

    @Test
    fun testSaveKwLabelHiddenStatusShouldCallRepository() {
        sharedPreferencesUseCases.saveKwLabelHiddenStatus(true)
        verify(sharedPreferenceRepository).saveKwLabelHiddenStatus(true)
    }

    @Test
    fun testSaveDistanceLabelHiddenStatusShouldCallRepository() {
        sharedPreferencesUseCases.saveDistanceLabelHiddenStatus(true)
        verify(sharedPreferenceRepository).saveDistanceLabelHiddenStatus(true)
    }

    @Test
    fun testClearPreferencesShouldCallRepository() {
        sharedPreferencesUseCases.clearPreferences()
        verify(sharedPreferenceRepository).clearPreferences()
    }
}