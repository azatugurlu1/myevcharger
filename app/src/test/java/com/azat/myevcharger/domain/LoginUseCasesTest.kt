package com.azat.myevcharger.domain

import com.azat.myevcharger.data.remote.LoginPostData
import com.azat.myevcharger.data.repositories.LoginRepositoryInterface
import com.azat.myevcharger.data.repositories.callbacks.AuthCallBack
import com.azat.myevcharger.domain.usecases.LoginUseCases
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify

class LoginUseCasesTest {
    @Mock
    private lateinit var loginRepository: LoginRepositoryInterface

    @Mock
    private lateinit var authCallBack: AuthCallBack

    private lateinit var loginUseCases: LoginUseCases

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        loginUseCases = LoginUseCases(loginRepository)
    }

    @Test
    fun testLoginShouldCallRepository() {
        loginUseCases.login("test", "test", authCallBack)
        val loginPostData = LoginPostData("test", "test")
        verify(loginRepository).login(loginPostData, authCallBack)
    }
}