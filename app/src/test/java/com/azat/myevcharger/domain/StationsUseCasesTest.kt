package com.azat.myevcharger.domain

import com.azat.myevcharger.data.remote.LoginPostData
import com.azat.myevcharger.data.repositories.StationsRepositoryInterface
import com.azat.myevcharger.data.repositories.callbacks.StationsCallBack
import com.azat.myevcharger.domain.usecases.StationsUseCases
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify

class StationsUseCasesTest {
    @Mock
    private lateinit var stationsRepository: StationsRepositoryInterface

    @Mock
    private lateinit var stationsCallBack: StationsCallBack

    private lateinit var stationsUseCases: StationsUseCases

    @Before
    fun before() {
        MockitoAnnotations.openMocks(this)
        stationsUseCases = StationsUseCases(stationsRepository)
    }

    @Test
    fun testGetStationsShouldCallRepository() {
        stationsUseCases.getStations(60.12, 10.13, stationsCallBack)
        verify(stationsRepository).getStations(60.12, 10.13, stationsCallBack)
    }


}